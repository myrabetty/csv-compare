import sys


def main(argv):
    if len(argv) < 2:
        print("Number of arguments: is", len(argv), "incorrect")
        sys.exit(1)
    compare_files_by_name(argv[0], argv[1])


def compare_files_by_name(file_name_one, file_name_two):
    with open(file_name_one) as file_one:
        # formatting old file to new file
        lines1 = set(line.replace("9999999;", "9999999.0;").replace("9999999.0;", "9999999999.0;").replace(".00;", ";").replace(".0;", ";").strip()
                     for line in file_one.readlines())

    with open(file_name_two) as file_two:
        lines2 = set(line.replace("9999999999.00;", "9999999999.0;").replace(".00;", ";").replace(".0;", ";").strip() for line in file_two.readlines())

    uniques = lines1 - lines2
    print("lines contained only in ", file_name_one)
    for line in uniques:
        print(line)


    uniques = lines2 - lines1
    print("lines contained only in ", file_name_two)
    for line in uniques:
        print(line)


compare_files_by_name("business_rules_20191112_092050.csv", "business_rule_20191120_095532.csv")


#if __name__ == "__main__":
#     main(sys.argv[1:])
